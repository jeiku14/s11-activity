package com.example.discussion.services;

import com.example.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {


    void createUser(User post);

    //We can iterate/loop over the posts in order to display them
    Iterable<User> getUsers();

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User post);
}
