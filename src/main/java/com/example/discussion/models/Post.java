package com.example.discussion.models;

import javax.annotation.processing.Generated;
import javax.persistence.*;

//Marks this Java object as a representation of an entity/record from the database table "posts"
@Entity
//Designates the table related to the model
@Table(name="posts")
public class Post {

    //Properties
    //Indicates that this property represents the primary key of the table
    @Id
    //Values for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    //Class properties that represent the table columns in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    //Constructors

    // Default constructors are required when retrieving data from the database
    public Post(){};

    public Post (String title, String content) {
        this.title = title;
        this.content = content;
    }

    //Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
